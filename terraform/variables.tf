variable "aws_region" {
  default = "us-west-2"
  type    = string
}
variable "aws_role" {
  default = "yellow_fish_lambda_role"
  type    = string
}
variable "project_name" {
  default = "yellow_fish_definition"
  type    = string
}
