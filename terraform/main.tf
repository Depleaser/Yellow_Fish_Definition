terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

resource "aws_iam_role" "yellow_fish_lambda_role" {
  name = var.aws_role

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "yellow_fish_function" {
  filename      = "project_yellow_fish.zip"
  function_name = var.project_name
  role          = aws_iam_role.yellow_fish_lambda_role.arn

  source_code_hash = filebase64sha256("project_yellow_fish.zip")

  runtime = "nodejs12.x"

}
