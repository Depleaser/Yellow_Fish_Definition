var sslChecker = require("ssl-checker");

// Client url to check if ssl cert is valid
let url = "www.client.com";
let expiryDate;
let todaysDate = new Date();

const sslCheck = () => {
    // Using a try catch statement so it fails gracefully
    try {
        sslChecker(url, 'GET', 443).then(
            result => {
                expiryDate = result.validTo;
                // all console.log statements need to be removed before prod
                console.log(expiryDate);
                console.log(todaysDate);
                // check if certificate is valid or expired
                if (todaysDate > expiryDate) {
                    console.log(`The SSL certificate has expired!`);
                } else {
                    console.log('This SSL certificate is valid!');
                }
            });
    } catch (error) {
        console.log('There was an error with the request', error)
    }
}
sslCheck();