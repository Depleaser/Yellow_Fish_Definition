# Yellow Fish Definition Project Solution

## Project Yellow Fish monitors the customer’s domains for SSL certificates - ensuring that they are always valid.

## Tools and technology used:

1. Terraform => IAC
2. Javascript => programming language
3. NPM => ssl checker package
4. SCM => Gitlab
5. CI/CD => Jenkins (coming soon)
6. AWS Lambda